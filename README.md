# simpleclient_tomcat-jdbc

Prometheus collector for metrics exposed by tomcat-jdbc connection pool.

## Using

### Assets

TODO: Add repository config for wherever I publish the library.

If you use maven you can simply add the following dependency to your build.

```xml
<groupId>com.gitlab.sharebear</groupId>
<artifactId>simpleclient_tomcat-jdbc</artifactId>
<version>1.0</version>
```

### Instrumenting

TODO: Document after I've written a test case or two.

## Contributing

### Code style

The google java formatter is run during the build using a maven plugin, therefore all code is
expected to be formatted according to the Google Style Guide.

### Commit comments

The intention is to follow the Angular style guide for commit comments, I apologise in advance if
there is any deviation.

## Maintenance

Feel free to file feature requests or bugs in the project issue tracker however I make no promises
maintain this library, it was created as an experiment before using it or a fork in my day job. If
you intend to use this in production systems I would recommend forking.

## License

TODO: Decide a license
