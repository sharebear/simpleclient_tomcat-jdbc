package com.gitlab.sharebear.prometheusclient.tomcatjdbc;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;
import java.util.Arrays;
import java.util.List;
import org.apache.tomcat.jdbc.pool.ConnectionPool;

public class TomcatJdbcCollector extends Collector {

  private final String connectionPoolName;
  private final ConnectionPool connectionPool;

  public TomcatJdbcCollector(String connectionPoolName, ConnectionPool connectionPool) {
    this.connectionPoolName = connectionPoolName;
    this.connectionPool = connectionPool;
  }

  public List<MetricFamilySamples> collect() {
    final GaugeMetricFamily connections =
        new GaugeMetricFamily(
            "tomcat_jdbc_connections",
            "The number of connections in the pool, both idle and active",
            asList("state", "pool_name"));
    connections.addMetric(asList("idle", connectionPoolName), connectionPool.getIdle());
    connections.addMetric(asList("active", connectionPoolName), connectionPool.getActive());

    final GaugeMetricFamily threadsWaiting =
        new GaugeMetricFamily(
            "tomcat_jdbc_threads_waiting",
            "The number of threads waiting for a connection",
            singletonList("pool_name"));
    threadsWaiting.addMetric(singletonList(connectionPoolName), connectionPool.getWaitCount());

    return Arrays.asList(connections, threadsWaiting);
  }
}
