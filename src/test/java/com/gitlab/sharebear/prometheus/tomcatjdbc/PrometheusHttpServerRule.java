package com.gitlab.sharebear.prometheusclient.tomcatjdbc;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.HTTPServer;
import java.net.InetSocketAddress;
import org.junit.rules.ExternalResource;

public class PrometheusHttpServerRule extends ExternalResource {

  private CollectorRegistry registry;
  private HTTPServer server;

  @Override
  public void before() throws Exception {
    registry = new CollectorRegistry();
    server = new HTTPServer(new InetSocketAddress(1234), registry);
  }

  @Override
  public void after() {
    server.stop();
  }

  public CollectorRegistry getRegistry() {
    return registry;
  }
}
