package com.gitlab.sharebear.prometheusclient.tomcatjdbc;

import static org.assertj.core.api.Assertions.*;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.junit.Rule;
import org.junit.Test;
import org.zapodot.junit.db.EmbeddedDatabaseRule;

public class TomcatJdbcCollectorTest {

  private final OkHttpClient client = new OkHttpClient();

  @Rule public PrometheusHttpServerRule prometheus = new PrometheusHttpServerRule();
  @Rule public EmbeddedDatabaseRule database = EmbeddedDatabaseRule.builder().build();

  // @Ignore
  @Test
  public void shouldReportEmptyMetricsWithNoPoolsRegistered() throws Exception {
    final Request request = new Request.Builder().url("http://localhost:1234/metrics").build();

    try (final Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      assertThat(response.body().string()).hasSize(0);
    }
  }

  @Test
  public void shouldReportSomething() throws Exception {
    final PoolProperties properties = new PoolProperties();
    properties.setUrl("jdbc:h2:mem:test");
    properties.setDriverClassName("org.h2.Driver");

    final DataSource dataSource = new DataSource();
    dataSource.setPoolProperties(properties);
    dataSource.getConnection();

    new TomcatJdbcCollector("A name", dataSource.getPool()).register(prometheus.getRegistry());

    final Request request = new Request.Builder().url("http://localhost:1234/metrics").build();

    try (final Response response = client.newCall(request).execute()) {
      assertThat(response.code()).isEqualTo(200);
      final String responseString = response.body().string();
      String[] lines = responseString.split("\n");
      assertThat(lines)
          .contains("tomcat_jdbc_connections{state=\"idle\",pool_name=\"A name\",} 9.0");
      assertThat(lines)
          .contains("tomcat_jdbc_connections{state=\"active\",pool_name=\"A name\",} 1.0");
      assertThat(lines).contains("tomcat_jdbc_threads_waiting{pool_name=\"A name\",} 0.0");
    }
  }
}
